/**
 * Created by dmitry on 10.05.2017.
 */
const mongoose = require('mongoose');
const User = mongoose.model('User');

const sendJsonResponse = function (res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.list = function (req, res) {
    User.find().exec((err,users) => {
        if (!users || users.length == 0) {
            sendJsonResponse(res, 404, {'message': 'Users not found'});
            return;
        } else if (err) {
            sendJsonResponse(res, 404, err);
            return;
        }
        sendJsonResponse(res, 200, users);
    });
};

module.exports.add = function (req, res) {
    if (req.body.name && req.body.age) {
        User.create({
            'name': req.body.name,
            'age': req.body.age
        }, (err, user) => {
            if (err) {
                sendJsonResponse(res, 400, err);
            } else {
                sendJsonResponse(res, 201, user);
            }
        });
    } else {
        sendJsonResponse(res, 400, {'message': 'Please fill fields \'name\' and \'age\''});
    }
};

module.exports.edit = function (req, res) {
    if (!req.params.id) {
        sendJsonResponse(res, 404, { 'message': 'Not found, \'id\' is required'});
        return;
    }
    if (!req.body.name || !req.body.age) {
        sendJsonResponse(res, 400, {'message': 'Please fill fields \'name\' and \'age\''});
        return;
    }
    User.findById(req.params.id).exec((err, user) => {
       if (err) {
           sendJsonResponse(res, 500, err);
       } else {
           user.name = req.body.name;
           user.age = req.body.age;
           user.save((err, user) => {
               if (err) {
                   sendJsonResponse(res, 404, err);
               } else {
                   sendJsonResponse(res, 200, user);
               }
           });
       }
    });
};

module.exports.deleteOne = function (req, res) {
    if (!req.params.id) {
        sendJsonResponse(res, 404, {'message': 'Not found, \'id\' is required'});
        return;
    }
    User.findByIdAndRemove(req.params.id).exec((err, user) => {
        if (err) {
            sendJsonResponse(res, 404, err);
        } else {
            sendJsonResponse(res, 204, user);
        }
    });
};