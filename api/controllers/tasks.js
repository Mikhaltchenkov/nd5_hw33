/**
 * Created by dmitry on 10.05.2017.
 */
const mongoose = require('mongoose');
const Task = mongoose.model('Task');

const sendJsonResponse = function (res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.list = function (req, res) {
    let findQuery = (req.query.find) ? {$or:[ {'name': new RegExp(req.query.find, 'i')},
            {'description': new RegExp(req.query.find, 'i')}]} : {};

    Task.find(findQuery).exec((err,tasks) => {
        if (!tasks || tasks.length == 0) {
        sendJsonResponse(res, 404, {'message': 'Tasks not found'});
        return;
    } else if (err) {
        sendJsonResponse(res, 404, err);
        return;
    }
    sendJsonResponse(res, 200, tasks);
});
};

module.exports.deleteOne = function (req, res) {
    if (!req.params.id) {
        sendJsonResponse(res, 404, {'message': 'Not found, \'id\' is required'});
        return;
    }
    Task.findByIdAndRemove(req.params.id).exec((err, task) => {
        if (err) {
            sendJsonResponse(res, 404, err);
        } else {
            sendJsonResponse(res, 204, task);
}
});
};

module.exports.add = function (req, res) {
    if (req.body.name && req.body.description && req.body.state) {
        Task.create({
            'name': req.body.name,
            'description': req.body.description,
            'state': req.body.state,
            'user': (req.body.user) ? req.body.user : null
        }, (err, task) => {
            if (err) {
                sendJsonResponse(res, 400, err);
            } else {
                sendJsonResponse(res, 201, task);
            }
        });
    } else {
        sendJsonResponse(res, 400, {'message': 'Please fill required fields \'name\', \'description\' and \'state\''});
    }
};

module.exports.edit = function (req, res) {
    if (!req.params.id) {
        sendJsonResponse(res, 404, { 'message': 'Not found, \'id\' is required'});
        return;
    }
    if (!req.body.name || !req.body.description || !req.body.user || !req.body.state) {
        sendJsonResponse(res, 400, {'message': 'Please fill required fields \'name\', \'description\', \'state\', \'user\''});
        return;
    }
    Task.findById(req.params.id).exec((err, task) => {
        if (err) {
            sendJsonResponse(res, 500, err);
        } else {
            task.name = req.body.name;
            task.description = req.body.description;
            task.user = req.body.user;
            task.state = req.body.state;

            task.save((err, task) => {
                if (err) {
                    sendJsonResponse(res, 404, err);
                } else {
                    sendJsonResponse(res, 200, task);
                }
            });
        }
    });
};
