/**
 * Created by dmitry on 10.05.2017.
 */
const mongoose = require('mongoose');

var taskSchema = new mongoose.Schema({
    name: String,
    description: String,
    state: Boolean,
    user: mongoose.Schema.ObjectId
});

mongoose.model('Task', taskSchema);