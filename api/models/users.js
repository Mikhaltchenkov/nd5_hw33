/**
 * Created by dmitry on 10.05.2017.
 */
const mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    name: String,
    age: Number
});
mongoose.model('User', userSchema);