/**
 * Created by dmitry on 10.05.2017.
 */
const mongoose = require('mongoose');
const uri = 'mongodb://127.0.0.1:27017/nd5_hw32';
mongoose.connect(uri);

require('./users');
require('./tasks');
