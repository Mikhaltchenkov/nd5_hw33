const express = require('express');
const app = module.exports = express();
require('./models/db');
const usersCtrl = require('./controllers/users');
const tasksCtrl = require('./controllers/tasks');

app.get('/users', usersCtrl.list);
app.post('/users', usersCtrl.add);
app.put('/users/:id', usersCtrl.edit);
app.delete('/users/:id', usersCtrl.deleteOne);

app.get('/tasks', tasksCtrl.list);
app.post('/tasks', tasksCtrl.add);
app.put('/tasks/:id', tasksCtrl.edit);
app.delete('/tasks/:id', tasksCtrl.deleteOne);
